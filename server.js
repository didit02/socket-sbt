const WebSocket = require('ws');
const wss = new WebSocket.Server({ port: 8080 });
console.log("Listening on 8080");

// waits for connection to be established from the client
// the callback argument ws is a unique for each client
wss.on('connection', (ws) => {
  // console.log("A user is connected");
  // runs a callback on message event
  ws.on('message', (data) => {
    // sends the data to all connected clients
    wss.clients.forEach((client) => {
        if (client.readyState === WebSocket.OPEN) {
          // console.log(data);
          client.send(data);
        }
    });
  });
});

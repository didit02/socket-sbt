var express=require('express');
var nodemailer = require("nodemailer");
const path = require('path');
var handlebars = require('handlebars');
var fs = require('fs');
const WebSocket = require('ws');
var app=express();


let smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: "digivlareport4@gmail.com",
        pass: "Rahasiadigivla!"
    }
});

var readHTMLFile = function(path, callback) {
    fs.readFile(path, {encoding: 'utf-8'}, function (err, html) {
        if (err) {
            throw err;
            callback(err);
        }
        else {
            callback(null, html);
        }
    });
};

function sendValues(){
  const ws = new WebSocket('ws://localhost:3002');
  ws.on('open', function open() {
    var obj = {status:false}
    var resp = JSON.stringify(obj);
    // console.log(typeof(resp))
    ws.send(resp);
  });

};

app.get('/test',function(req,res){
  res.sendFile(__dirname + '/templates/email.html');
});
app.post('/send',function(req,res){
  readHTMLFile(__dirname + '/templates/email.html', function(err, html) {
    var template = handlebars.compile(html);
    var htmlToSend = template(template);
    host=req.get('host');
    link="http://"+req.get('host')+"/alert/disable";
    mailOptions={
        to : req.query.to,
        subject : "Notifikasi dari EWS System",
        html:htmlToSend
        // html : "Hello,<br> This Alert from EWS System.<br>to disable alert on dashboard click link down below <br> <a href="+link+">Click here</a>"
    }
    // console.log(mailOptions);
    smtpTransport.sendMail(mailOptions, function(error, response){
     if(error){
        console.log(error);
        res.end("error");
     }else{
        console.log("Email sent");
        res.send({status:'sent'});
         }
});
});
});

app.get('/alert/disable',function(req,res){
  sendValues();
  res.send({status:false});
  res.end();


//         rand=Math.floor((Math.random() * 100) + 54);
//     host=req.get('host');
//     link="http://"+req.get('host')+"/verify?id="+rand;
//     mailOptions={
//         to : req.query.to,
//         subject : "Alert from EWS System",
//         html : "Hello,<br> This alert from EWS System.<br><a href="+link+">Click here to to disable alert on dashboard</a>"
//     }
//     console.log(mailOptions);
//     smtpTransport.sendMail(mailOptions, function(error, response){
//      if(error){
//             console.log(error);
//         res.end("error");
//      }else{
//             console.log("Message sent: " + response.message);
//         res.end("sent");
//          }
// });
});

app.listen(3001,function(){
    console.log("Express Started on Port 3001");
});

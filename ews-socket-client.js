const WebSocket = require('ws');

const ws = new WebSocket('ws://localhost:3002');

ws.on('open', function open() {
  console.log('connected');
  // ws.send(Date.now());
});

ws.on('close', function close() {
  console.log('disconnected');
});

ws.on('message', function incoming(data) {
  // resp = {"period":data}
  console.log(JSON.parse(data));

  // setTimeout(function timeout() {
  //   ws.send(Date.now());
  // }, 500);
});
